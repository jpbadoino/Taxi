@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('driver.index') }}
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="offset-md-4 col-md-4 col-12">
                    <a class="btn btn-success btn-block" href="{{ route('driver.create') }}">Dodaj nowego kierowcę</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th>Lp.</th>
                        <th>Imię i nazwisko</th>
                        <th>Numer telefonu</th>
                        <th>Adres e-mail</th>
                        <th class="col-1"></th>
                        <th class="col-1"></th>
                        <th class="col-1"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($drivers as $driver)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $driver->name }}</td>
                            <td>{{ $driver->phone }}</td>
                            <td>{{ $driver->email }}</td>
                            <td><a href="{{ route('driver.show', $driver) }}" class="btn btn-warning btn-block">Pokaż</a></td>
                            <td><a href="{{ route('driver.edit', $driver) }}" class="btn btn-primary btn-block">Edytuj</a></td>
                            <td><a href="{{ route('driver.destroy', $driver) }}" class="btn btn-danger btn-block">Usuń</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{ $drivers->links() }}
    </div>
</div>
@endsection
