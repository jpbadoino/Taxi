@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('driver.edit', $driver) }}
    {{ Form::model($driver, ['route' => ['driver.update', $driver], 'method' => 'PUT']) }}
        <div class="row">
            <div class="col-8">
                <div class="card">
                    <div class="card-body">
                        <h3>Kierowca</h3>
                        @include('driver.inputs.driver')
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <h3>Prawo jazdy</h3>
                        @include('driver.inputs.license', [
                            'license' => $driver->driverLicense,
                        ])
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                @include('layouts.form-button', [
                    'text' => 'Edytuj »',
                ])
            </div>
        </div>
    {{ Form::close() }}
</div>
@endsection
