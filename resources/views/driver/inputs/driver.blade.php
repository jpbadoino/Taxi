<div class="row">
    <div class="col-6">
        <div class="form-group">
            {{ Form::label('driver-first_name', 'Imię', ['class' => 'form-label']) }}
            {{ Form::text('driver[first_name]', $driver->first_name, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            {{ Form::label('driver-last_name', 'Nazwisko', ['class' => 'form-label']) }}
            {{ Form::text('driver[last_name]', $driver->last_name, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            {{ Form::label('driver-email', 'Adres e-mail', ['class' => 'form-label']) }}
            {{ Form::text('driver[email]', $driver->email, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            {{ Form::label('driver-phone', 'Numer telefonu', ['class' => 'form-label']) }}
            {{ Form::text('driver[phone]', $driver->phone, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            {{ Form::label('driver-address_1', 'Ulica', ['class' => 'form-label']) }}
            {{ Form::text('driver[address_1]', $driver->address_1, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-3">
        <div class="form-group">
            {{ Form::label('driver-address_2', 'Numer domu', ['class' => 'form-label']) }}
            {{ Form::text('driver[address_2]', $driver->address_2, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-3">
        <div class="form-group">
            {{ Form::label('driver-address_3', 'Numer lokalu', ['class' => 'form-label']) }}
            {{ Form::text('driver[address_3]', $driver->address_3, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-3">
        <div class="form-group">
            {{ Form::label('driver-postal_code', 'Kod pocztowy', ['class' => 'form-label']) }}
            {{ Form::text('driver[postal_code]', $driver->postal_code, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-3">
        <div class="form-group">
            {{ Form::label('city', 'Miasto', ['class' => 'form-label']) }}
            {{ Form::text('driver[city]', $driver->city, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            {{ Form::label('driver-country', 'Państwo', ['class' => 'form-label']) }}
            {{ Form::text('driver[country]', $driver->country ?: 'Polska', ['class' => 'form-control']) }}
        </div>
    </div>
</div>
