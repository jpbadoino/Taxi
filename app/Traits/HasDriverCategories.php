<?php

namespace App\Traits;

trait HasDriverCategories
{
    public function getDriverCategories()
    {
        return (object) [
            'B'  => '[B] Samochód osobowy',
            'BE' => '[BE] Samochód osobowy z przyczepą',
        ];
    }
}
