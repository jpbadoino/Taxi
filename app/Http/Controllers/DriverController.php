<?php

namespace App\Http\Controllers;

use App\Http\Requests\DriverRequest;
use App\Traits\HasDriverCategories;
use App\DriverLicense;
use App\Driver;

class DriverController extends Controller
{
    use HasDriverCategories;

    public function index()
    {
        $drivers = Driver::paginate();

        return view('driver.index', [
            'drivers' => $drivers,
        ]);
    }

    public function create()
    {
        $categories = $this->getDriverCategories();

        return view('driver.create', [
            'categories' => $categories,
        ]);
    }

    public function store(DriverRequest $request)
    {
        $driver = Driver::create($request->input('driver'));
        $driver->driverLicense()->create($request->input('license'));

        return redirect()->route('driver.index')->withFlash('Pomyślnie dodano kierowcę');
    }

    public function show(Driver $driver)
    {
        return view('driver.show', [
            'driver' => $driver,
        ]);
    }

    public function edit(Driver $driver)
    {
        $categories = $this->getDriverCategories();

        return view('driver.edit', [
            'driver'     => $driver,
            'categories' => $categories,
        ]);
    }

    public function update(DriverRequest $request, Driver $driver)
    {
        $driver->update($request->input('driver'));
        $driver->driverLicense->update($request->input('license'));

        return redirect()->route('driver.index')->withFlash('Pomyślnie zaktualizowano kierowcę');
    }

    public function destroy(Driver $driver)
    {
        $driver->delete();

        return redirect()->route('driver.index')->withFlash('Pomyślnie usunięto kierowcę');
    }
}
