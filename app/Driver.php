<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;

class Driver extends Model
{
    //use FormAccessible;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name',
        'email', 'phone',
        'address_1', 'address_2', 'address_3',
        'postal_code', 'city', 'country',
    ];

    protected $with = [
        'driverLicense',
    ];

    public function driverLicense()
    {
        return $this->hasOne(DriverLicense::class);
    }

    public function getNameAttribute()
    {
        return sprintf('%s %s', $this->first_name, $this->last_name);
    }

    public function getAddressAttribute()
    {
        $pattern = $this->address_3 ? '%s %s/%s' : '%s %s';

        return sprintf($pattern, $this->address_1, $this->address_2, $this->address_3);
    }
}
