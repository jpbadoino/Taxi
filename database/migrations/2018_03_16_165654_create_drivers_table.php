<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 25);
            $table->string('last_name', 50);
            $table->string('email')->nullable();
            $table->string('phone', 10)->nullable();
            $table->string('address_1', 50);
            $table->string('address_2', 10);
            $table->string('address_3', 10)->nullable();
            $table->string('city', 25);
            $table->string('postal_code', 10);
            $table->string('country', 25);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
